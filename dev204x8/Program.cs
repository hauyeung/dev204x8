﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dev204x8
{
    class Program
    {
        static void Main(string[] args)
        {
            UProgram program = new UProgram();
            program.degree = new Degree();
            Course[] courses = new Course[3];
            courses[0] = new Course() { name = "Programming with C#" };
            courses[1] = new Course() { name = "Programming with Visual Basic" };
            courses[1] = new Course() { name = "Programming with C" };
            program.degree.courses = courses;
            program.name = "Information Technology";
            program.degree.name = "Bachelor of Technology";
            List<Student> students = new List<Student>();
            Stack<int> grades = new Stack<int>();
            grades.Push(90);
            grades.Push(80);
            students.Add(new Student() { firstname = "John", lastname = "Doe", grades = grades });
            students.Add(new Student() { firstname = "Sukie", lastname = "Jeffery", grades = grades });
            students.Add(new Student() { firstname = "Hania", lastname = "Lu", grades = grades });
            program.degree.courses[0].students = students;
            program.degree.courses[0].teacher = new Teacher() { firstname = "Marita", lastname = "Verona" };
            Console.WriteLine("The {0} program contains the {1}", program.name, program.degree.name);
            Console.WriteLine();
            Console.WriteLine("The {0} degree contains the course {1}", program.degree.name, program.degree.courses[0].name);
            Console.WriteLine();
            Console.WriteLine("The {0} course contains {1} student(s)", program.degree.courses[0].name, program.degree.courses[0].students.Count);
            Console.WriteLine();
            courses[0].ListStudents();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
