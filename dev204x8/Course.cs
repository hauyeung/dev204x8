﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dev204x8
{
    class Course
    {

        public List<Student> students;
        public Teacher teacher;
        public string name;
        public void ListStudents()
        {
            Console.WriteLine("Students in this course include:");
            foreach (Student student in students)
            {                
                Console.WriteLine("{0} {1}", student.firstname, student.lastname);
                Console.WriteLine();
            }
        }
    }
}
